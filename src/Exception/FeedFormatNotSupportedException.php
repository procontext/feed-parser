<?php

namespace Procontext\FeedParser\Exception;

use Throwable;

class FeedFormatNotSupportedException extends \Exception
{
    public function __construct($message = "Указанный формат файла фида не поддерживается", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}